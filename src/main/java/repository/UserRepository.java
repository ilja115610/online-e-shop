package repository;

import domain.User;
import service.DBService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class UserRepository {

    private final EntityManagerFactory entityManagerFactory = DBService.getEntityManagerFactory();

    public void saveUser(User user) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(user);
            em.getTransaction().commit();
            System.out.println("User added!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void updateUser(User user) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.merge(user);
            em.getTransaction().commit();
            System.out.println("User updated!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void deleteUser(int id) {

        String sql = "DELETE User where id = :userId";

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        int result = em.createQuery(sql).setParameter("userId", id).executeUpdate();

        em.getTransaction().commit();
        if (result > 0) {
            System.out.println("User deleted!");
        }
    }

    public List<User> getUsers() {

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        return em.createQuery("from User ", User.class).getResultList();
    }

    public void sayHello () {
        System.out.println("Hello World!");
    }

}
