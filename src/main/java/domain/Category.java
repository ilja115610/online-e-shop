package domain;

import org.hibernate.annotations.SortNatural;

import javax.persistence.*;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "category")
public class Category implements Comparable<Category>{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "groupp_id")
    private Group groupp;

    @OneToMany(mappedBy = "category")
    @SortNatural
    private Set<Product> products = new TreeSet<>();

    public Category(String name, Group groupp) {
        this.name = name;
        this.groupp = groupp;
    }

    public Category() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Group getGroupp() {
        return groupp;
    }

    public void setGroupp(Group groupp) {
        this.groupp = groupp;
    }

    @Override
    public int compareTo(Category o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return
                "ID: " + id +
                ", NAME: '" + name + '\'' +
                '}';
    }


}
