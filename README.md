## ONLINE E-SHOP

### Tools & Technologies:

* Java 8+

* Maven

* Hibernate 5.4.10 + MySQL  

# WORKFLOW

* User gets initially to Login menu and have 3 options:
Login (existing user); Register (new user); exit.
  
* User gets to main menu once logged in or registered.
  There are 3 options in main menu:
  * Select product (starting from group->category->product),
  * Checkout (once product added to basket),
  * Display all orders related to logged-in user
  * Exit

    ## Installation
    
    Clone repository: `https://gitlab.com/ilja115610/online-e-shop`
    
    Build: `mvn clean install`
    
    